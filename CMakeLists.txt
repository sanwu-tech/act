cmake_minimum_required(VERSION 2.8)
project(act)

# Path configuration
set(PROJECT_SOURCE_DIR ${PROJECT_SOURCE_DIR}/src)

# Specify library dependencies
find_library(act REQUIRED)

# Create executable
add_executable(main ${PROJECT_SOURCE_DIR}/main.c)
set_target_properties(main PROPERTIES OUTPUT_NAME act)
target_include_directories(main PUBLIC ${act_INCLUDE_DIR})
target_link_libraries(main act)

# Install
install(TARGETS main DESTINATION bin)

# Uninstall
CONFIGURE_FILE(
	"${CMAKE_CURRENT_BINARY_DIR}/cmake_uninstall.cmake"
	IMMEDIATE @ONLY)
ADD_CUSTOM_TARGET(uninstall
	"${CMAKE_COMMAND}" -P "${CMAKE_CURRENT_BINARY_DIR}/cmake_uninstall.cmake") 
