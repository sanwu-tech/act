# ACT [![Build Status](https://travis-ci.org/sanwu-tech/act.svg?branch=master)](https://travis-ci.org/sanwu-tech/act)

act is a tool for communicating with ACT card dispensers.

You can download pre-compiled binary release from https://github.com/sanwu-tech/act/releases.

## Run
1. Run `./act -p [PORT]` or `./act -p [PORT] -b [BAUDRATE]`

## Build
1. Install [libact](https://bitbucket.org/sanwu-tech/libact)
2. Run `cmake .`, `make`, `make install`
