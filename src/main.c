#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <dirent.h>
#include <errno.h>
#include <getopt.h>
#include <signal.h>

#include <act.h>

static struct act *act;
static int running = 1;

/* Options */
static char *port;
static int   baudrate;

static const char HELP[] =
"\n"
"act is a tool for communicating with ACT card dispensers.\n"
"\n"
"Usage: act [PARAMS]\n"
"\n"
"List of options:\n"
"-b [BAUDRATE]                set baudrate for the device\n"
"                             (only 9600, 19200, 38400, and 57600 are valid)\n"
"\n"
"-h                           print this help\n"
"\n"
"-p [PORT]                    set port of the device (e.g. /dev/ttyUSB0)\n"
"\n";

static const char COMMAND_HELP[] = 
"\n"
"list of commands\n"
"----------------\n"
"reset                     initialize card dispenser with optional action\n"
"status                    check current status of the card dispenser\n"
"move                      move the card to ic / rf slot, collection box, exit or outlet\n"
"set                       permit or deny card insertion\n"
"detect                    detect type of ic / rf card\n"
"cpu                       (not implemented)\n"
"sam                       (not implemented)\n"
"sle                       (not implemented)\n"
"iic                       (not implemented)\n"
"rf                        rf card various operations such as activate, status, read / write\n"
"info                      show information about the card dispenser\n"
"card-serial               (need description)\n"
"card-version              (need description)\n"
"recovery-card             (not implemented)\n"
"quit                      quit the program\n";

static void not_implemented()
{
	fprintf(stdout, "not implemented\n");
}

static char * next_arg(char **s)
{
	char *p, *q = *s;

	for (p = *s; *p != ' ' && *p; p++)
		;
	*p = 0;

	if (*s)
		*s = p + 1;

	return q;
}

static int count_tokens(char *s, char sep)
{
	int cnt = *s ? 1 : 0;

	for (; *s != 0; s++) {
		if (*s == sep)
			cnt++;
	}

	return cnt;
}

static void read_hex_string(char *p, unsigned char *hex, int hex_length)
{
	int i;

	for (i = 0; i < hex_length; i++) {
		const char *q = next_arg(&p);
		sscanf(q, "%hhx", &hex[i]);
	}
}

static int find_serial_port(char **ttypath)
{
	DIR *dir;
	struct dirent *ent;
	int ret = 0;

	if ((dir = opendir("/dev")) == NULL) {
		ret = -errno;
		fprintf(stderr, "failed to list devices: %s\n", strerror(-ret));
		goto out;
	}

	while ((ent = readdir(dir)) != NULL) {
		if (strncmp(ent->d_name, "ttyUSB", 6) == 0 ||
		    strncmp(ent->d_name, "ttyACM", 6) == 0) {
			*ttypath = malloc(strlen("/dev/") + strlen(ent->d_name) + 1);
			*ttypath[0] = 0;
			strcat(*ttypath, "/dev/");
			strcat(*ttypath, ent->d_name);
			closedir(dir);
			break;
		}
	}

	if (!(*ttypath)) {
		ret = -ENODEV;
		fprintf(stderr, "failed to find a suitable device: %s\n", strerror(-ret));
		goto out;
	}

out:
	return ret;
}

static void command_reset(char *p)
{
	char *command = next_arg(&p);
	struct response r;

	if (strcmp("move", command) == 0) {
		act_initialize(act, &r, RESET_CARD_MOVE, 0, 0);
	} else if (strcmp("collect", command) == 0) {
		act_initialize(act, &r, RESET_CARD_COLLECT, 0, 0);
	} else if (strcmp("without-move", command) == 0) {
		act_initialize(act, &r, RESET_CARD_WITHOUT_MOVE, 0, 0);
	} else if (strcmp("move-with-count", command) == 0) {
		act_initialize(act, &r, RESET_CARD_MOVE_WITH_COUNT, 0, 0);
	} else if (strcmp("collect-with-count", command) == 0) {
		act_initialize(act, &r, RESET_CARD_COLLECT_WITH_COUNT, 0, 0);
	} else if (strcmp("without-move-with-count", command) == 0) {
		act_initialize(act, &r, RESET_CARD_WITHOUT_MOVE_WITH_COUNT, 0, 0);
	} else if (strcmp("help", command) == 0 || strlen(command) == 0) {
		fprintf(stdout, "\n%s\n",
			"list of parameters\n"
			"------------------\n"
			"move\n"
			"collect\n"
			"without-move\n"
			"move-with-count\n"
			"collect-with-count\n"
			"without-move-with-count\n"
		);
		return;
	} else {
		fprintf(stderr, "invalid parameter '%s'\n", p);
		return;
	}

	act_print_message(&r);
}

static void command_status(char *p)
{
	struct response r;
	char *command = next_arg(&p);

	if (strcmp("basic", command) == 0) {
		act_get_crs_status(act, &r, 0);
	} else if (strcmp("basic-se", command) == 0) {
		act_get_sensors_status(act, &r, 0);
	} else if (strcmp("help", p) == 0 || strlen(p) == 0) {
		fprintf(stdout, "\n%s\n",
			"list of parameters\n"
			"------------------\n"
			"basic\n"
			"basic-se\n"
		);
		return;
	} else {
		fprintf(stderr, "invalid parameter '%s'\n", command);
		return;
	}

	act_print_message(&r);
}

static void command_move_card(char *p)
{
	char *command = next_arg(&p);
	struct response r;

	if (strcmp("exit", command) == 0) {
		act_move_card(act, &r, MOVE_CARD_TO_EXIT);
	} else if (strcmp("ic-slot", command) == 0) {
		act_move_card(act, &r, MOVE_CARD_TO_IC_SLOT);
	} else if (strcmp("rf-slot", command) == 0) {
		act_move_card(act, &r, MOVE_CARD_TO_RF_SLOT);
	} else if (strcmp("collection", command) == 0) {
		act_move_card(act, &r, MOVE_CARD_TO_COLLECTION);
	} else if (strcmp("outlet", command) == 0) {
		act_move_card(act, &r, MOVE_CARD_TO_OUTLET);
	} else if (strcmp("help", command) == 0 || strlen(command) == 0) {
		fprintf(stdout, "\n%s\n",
			"list of parameters\n"
			"------------------\n"
			"exit\n"
			"ic-slot\n"
			"rf-slot\n"
			"collection\n"
			"outlet\n"
		);
		return;
	} else {
		fprintf(stderr, "invalid parameter '%s'\n", command);
		return;
	}

	act_print_message(&r);
}

static void command_set_card(char *p)
{
	char *command = next_arg(&p);
	struct response r;

	if (strcmp("permit", command) == 0) {
		act_permit_insertion(act, &r);
	} else if (strcmp("deny", command) == 0) {
		act_deny_insertion(act, &r);
	} else if (strcmp("help", command) == 0 || strlen(command) == 0) {
		fprintf(stdout, "\n%s\n",
			"list of parameters\n"
			"------------------\n"
			"permit\n"
			"deny\n"
		);
		return;
	} else {
		fprintf(stderr, "invalid parameter '%s'\n", command);
		return;
	}

	act_print_message(&r);
}

static void command_detect(char *p)
{
	char *command = next_arg(&p);
	struct response r;

	if (strcmp("ic", command) == 0) {
		act_detect_ic(act, &r, 0);
	} else if (strcmp("rf", command) == 0) {
		act_detect_rf(act, &r, 0);
	} else if (strcmp("help", command) == 0 || strlen(command) == 0) {
		fprintf(stdout, "\n%s\n",
			"list of parameters\n"
			"------------------\n"
			"ic\n"
			"rf\n"
		);
		return;
	} else {
		fprintf(stderr, "invalid parameter '%s'\n", command);
		return;
	}

	act_print_message(&r);
}

static void command_rf_card_activate(char *p)
{
	char *command = next_arg(&p);
	struct response r;
	unsigned char protocols[2];
	int ret;

	if (strcmp("a-b", command) == 0) {
		protocols[0] = 'A';
		protocols[1] = 'B';
	} else if (strcmp("a-m", command) == 0) {
		protocols[0] = 'A';
		protocols[1] = 'M';
	} else if (strcmp("b-a", command) == 0) {
		protocols[0] = 'B';
		protocols[1] = 'A';
	} else if (strcmp("b-m", command) == 0) {
		protocols[0] = 'B';
		protocols[1] = 'M';
	} else if (strcmp("m-a", command) == 0) {
		protocols[0] = 'M';
		protocols[1] = 'A';
	} else if (strcmp("m-b", command) == 0) {
		protocols[0] = 'M';
		protocols[1] = 'B';
	} else if (strcmp("help", command) == 0 || strlen(command) == 0) {
		fprintf(stdout, "\n%s\n",
			"list of parameters\n"
			"------------------\n"
			"a-b\n"
			"a-m\n"
			"b-a\n"
			"b-m\n"
			"m-a\n"
			"m-b\n"
		);
		return;
	} else {
		fprintf(stderr, "invalid parameter '%s'\n", command);
		return;
	}

	if ((ret = act_rf_activate(act, &r, protocols[0], protocols[1], 0, 0)) < 0)
		goto out;

out:
	act_print_message(&r);
}

static void command_rf_card_mifare(char *p)
{
	char *command = next_arg(&p);
	struct response r;
	char *key_select_s = 0;
	char *number_s;
	int use_key_a;
	unsigned char mifare_command;
	unsigned char password[6];
	unsigned char block_data[16];
	unsigned char value[4];
	unsigned int sector;
	unsigned int block;
	unsigned int block_count;
	unsigned int block_size;

	if (strcmp(command, "verify-password") == 0) {
		mifare_command = MIFARE_PASSWORD_VERIFICATION;

	} else if (strcmp(command, "load-password") == 0) {
		mifare_command = MIFARE_LOAD_CRYPTO_CHECKSUM_FROM_EEPROM;

	} else if (strcmp(command, "update-password") == 0) {
		mifare_command = MIFARE_MODIFY_SECTOR_PASSWORD_KEY_A;

	} else if (strcmp(command, "download-password") == 0) {
		mifare_command = MIFARE_DOWNLOAD_PASSWORD_TO_EEPROM;

	} else if (strcmp(command, "read-sector") == 0) {
		mifare_command = MIFARE_READ_SECTOR_BLOCK_DATA;

	} else if (strcmp(command, "write-sector") == 0) {
		mifare_command = MIFARE_WRITE_SECTOR_BLOCK_DATA;

	} else if (strcmp(command, "initialize") == 0) {
		mifare_command = MIFARE_INITIALIZE;

	} else if (strcmp(command, "read") == 0) {
		mifare_command = MIFARE_READ;

	} else if (strcmp(command, "increment") == 0) {
		mifare_command = MIFARE_INCREMENT;

	} else if (strcmp(command, "decrement") == 0) {
		mifare_command = MIFARE_DECREMENT;

	} else if (strcmp(command, "help") == 0 || strlen(command) == 0) {
		fprintf(stdout, "\n%s\n",
			"list of parameters\n"
			"------------------\n"
			"verify-password\n"
			"load-password\n"
			"update-password\n"
			"download-password\n"
			"read-sector\n"
			"write-sector\n"
			"initialize\n"
			"read\n"
			"increment\n"
			"decrement\n"
		);
		return;
	} else {
		goto invalid;
	}

	/* First Argument */
	switch (mifare_command) {
	case MIFARE_PASSWORD_VERIFICATION:
	case MIFARE_LOAD_CRYPTO_CHECKSUM_FROM_EEPROM:
	case MIFARE_DOWNLOAD_PASSWORD_TO_EEPROM:
		key_select_s = next_arg(&p);
		if (!((*key_select_s == 'a' || *key_select_s == 'A') &&
		      (*key_select_s != 'b' || *key_select_s != 'B')))
			goto invalid;

		break;

	case MIFARE_MODIFY_SECTOR_PASSWORD_KEY_A:
	case MIFARE_READ_SECTOR_BLOCK_DATA:
	case MIFARE_WRITE_SECTOR_BLOCK_DATA:
	case MIFARE_INITIALIZE:
	case MIFARE_READ:
	case MIFARE_INCREMENT:
	case MIFARE_DECREMENT:
		number_s = next_arg(&p);
		errno = 0;

		sscanf(number_s, "%x", &sector);
		if (errno || sector > 0x0F)
			goto invalid;

		break;
	}

	/* Second Argument */
	switch (mifare_command) {
	case MIFARE_PASSWORD_VERIFICATION:
	case MIFARE_LOAD_CRYPTO_CHECKSUM_FROM_EEPROM:
	case MIFARE_DOWNLOAD_PASSWORD_TO_EEPROM:
		number_s = next_arg(&p);
		errno = 0;

		sscanf(number_s, "%x", &sector);
		if (errno || sector > 0x0F)
			goto invalid;

		break;

	case MIFARE_MODIFY_SECTOR_PASSWORD_KEY_A:
		if (count_tokens(p, ' ') < sizeof(password))
			goto invalid;

		read_hex_string(p, password, sizeof(password));
		break;

	case MIFARE_READ_SECTOR_BLOCK_DATA:
	case MIFARE_WRITE_SECTOR_BLOCK_DATA:
	case MIFARE_INITIALIZE:
	case MIFARE_READ:
	case MIFARE_INCREMENT:
	case MIFARE_DECREMENT:
		number_s = next_arg(&p);
		errno = 0;

		sscanf(number_s, "%x", &block);
		if (errno)
			goto invalid;

		break;
	}

	/* Third Argument */
	switch (mifare_command) {
	case MIFARE_PASSWORD_VERIFICATION:
	case MIFARE_DOWNLOAD_PASSWORD_TO_EEPROM:
		if (count_tokens(p, ' ') != 6)
			goto invalid;

		read_hex_string(p, password, sizeof(password));
		break;

	case MIFARE_READ_SECTOR_BLOCK_DATA:
	case MIFARE_WRITE_SECTOR_BLOCK_DATA:
		number_s = next_arg(&p);
		errno = 0;

		sscanf(number_s, "%x", &block_count);
		if (errno)
			goto invalid;
		break;
	}

	/* Fourth Argument */
	switch (mifare_command) {
	case MIFARE_WRITE_SECTOR_BLOCK_DATA:
		if ((block_size = count_tokens(p, ' ')) > sizeof(block_data))
			goto invalid;

		read_hex_string(p, block_data, block_size);
		break;

	case MIFARE_INITIALIZE:
	case MIFARE_INCREMENT:
	case MIFARE_DECREMENT:
		if (count_tokens(p, ' ') != sizeof(value))
			goto invalid;

		read_hex_string(p, value, sizeof(value));
		break;
	}

	if (key_select_s) {
		if (*key_select_s == 'a' || *key_select_s == 'A')
			use_key_a = 1;
	}

	switch (mifare_command) {
	case MIFARE_PASSWORD_VERIFICATION:
		act_rf_mifare_verify_password(act, &r, use_key_a, sector, &password);
		break;

	case MIFARE_LOAD_CRYPTO_CHECKSUM_FROM_EEPROM:
		act_rf_mifare_load_password(act, &r, use_key_a, sector);
		break;

	case MIFARE_MODIFY_SECTOR_PASSWORD_KEY_A:
		act_rf_mifare_update_password(act, &r, sector, &password);
		break;

	case MIFARE_DOWNLOAD_PASSWORD_TO_EEPROM:
		act_rf_mifare_download_password(act, &r, use_key_a, sector, &password);
		break;

	case MIFARE_READ_SECTOR_BLOCK_DATA:
		act_rf_mifare_read_sector(act, &r, sector, block, block_count, 0, 0);
		break;

	case MIFARE_WRITE_SECTOR_BLOCK_DATA:
		act_rf_mifare_write_sector(act, &r, sector, block, block_count, block_data, block_size);
		break;

	case MIFARE_INITIALIZE:
		act_rf_mifare_initialize_value(act, &r, sector, block, &value);
		break;

	case MIFARE_READ:
		act_rf_mifare_read_value(act, &r, sector, block, &value);
		break;

	case MIFARE_INCREMENT:
		act_rf_mifare_increment_value(act, &r, sector, block, &value);
		break;

	case MIFARE_DECREMENT:
		act_rf_mifare_decrement_value(act, &r, sector, block, &value);
		break;
	}

	act_print_message(&r);
	return;

invalid:
	fprintf(stderr, "invalid arguments\n");
}

static void command_rf_card(char *p)
{
	char *command = next_arg(&p);
	struct response r;

	if (strcmp("activate", command) == 0) {
		command_rf_card_activate(p);
		return;

	} else if (strcmp("release", command) == 0) {
		act_rf_release(act, &r);
		goto out;

	} else if (strcmp("status", command) == 0) {
		act_rf_status(act, &r, 0);
		goto out;

	} else if (strcmp("mifare", command) == 0) {
		command_rf_card_mifare(p);
		return;

	} else if (strcmp("type-a", command) == 0) {
		not_implemented();
		return;

	} else if (strcmp("type-b", command) == 0) {
		not_implemented();
		return;

	} else if (strcmp("wake-sleep", command) == 0) {
		act_rf_wake_sleep(act, &r);

	} else if (strcmp("help", command) == 0 || strlen(command) == 0) {
		fprintf(stdout, "\n%s\n",
			"list of parameters\n"
			"------------------\n"
			"activate\n"
			"release\n"
			"status\n"
			"mifare\n"
			"type-a                    (not implemented)\n"
			"type-b                    (not implemented)\n"
			"wake-sleep\n"
		);
		return;

	} else {
		fprintf(stderr, "invalid parameter '%s'\n", command);
		return;
	}

out:
	act_print_message(&r);
}

static void command_card_serial(char *p)
{
	struct response r;

	act_card_serial_number(act, &r, 0, 0);
	act_print_message(&r);
}

static void command_info(char *s)
{
	struct response r;

	act_reader_info(act, &r, 0);
	act_print_message(&r);
}

static void command_card_version(char *p)
{
	struct response r;
	char *command = next_arg(&p);

	if (strcmp(command, "auto") == 0) {
		act_version_info(act, &r, VERSION_INFO_READ);
	} else if (strcmp(command, "ic") == 0) {
		act_version_info(act, &r, VERSION_INFO_READ_IC);
	} else if (strcmp(command, "rf") == 0) {
		act_version_info(act, &r, VERSION_INFO_READ_RF);
	} else if (strcmp(command, "help") == 0 || strlen(p) == 0) {
		fprintf(stdout, "\n%s\n",
			"list of parameters\n"
			"------------------\n"
			"auto\n"
			"ic\n"
			"rf\n"
		);
		return;
	} else {
		fprintf(stderr, "invalid parameter '%s'\n", command);
		return;
	}

	act_print_message(&r);
}

static void print_help()
{
	printf("%s\n", HELP);
}

static void parse_args(char *args)
{
	char *command = next_arg(&args);

	if (strcmp("reset", command) == 0)
		command_reset(args);
	else if (strcmp("status", command) == 0)
		command_status(args);
	else if (strcmp("move", command) == 0)
		command_move_card(args);
	else if (strcmp("set", command) == 0)
		command_set_card(args);
	else if (strcmp("detect", command) == 0)
		command_detect(args);
	else if (strcmp("cpu", command) == 0)
		not_implemented();
	else if (strcmp("sam", command) == 0)
		not_implemented();
	else if (strcmp("sle", command) == 0)
		not_implemented();
	else if (strcmp("iic", command) == 0)
		not_implemented();
	else if (strcmp("rf", command) == 0)
		command_rf_card(args);
	else if (strcmp("info", command) == 0)
		command_info(args);
	else if (strcmp("card-serial", command) == 0)
		command_card_serial(args);
	else if (strcmp("card-version", command) == 0)
		command_card_version(args);
	else if (strcmp("recovery-card", command) == 0)
		not_implemented();
	else if (strcmp("help", command) == 0)
		fprintf(stdout, "%s\n", COMMAND_HELP);
	else if (strcmp("quit", command) == 0)
		running = 0;
	else
		fprintf(stderr, "invalid command '%s'\n", command);
}

static int process()
{
	char args[64] = { 0 };

	fprintf(stdout, "> ");
	fgets(args, sizeof(args), stdin);
	args[strlen(args) - 1] = 0;
	parse_args(args);

	return 0;
}

static void parse_options(int argc, char **argv)
{
	int c;
	int ret = 0;

	opterr = 0;

	while ((c = getopt(argc, argv, "hb:p:")) != -1) {
		switch (c) {
		case 'h':
			print_help();
			exit(0);
			break;

		case 'p':
			port = optarg;
			break;

		case 'b':
			baudrate = atoi(optarg);

			switch (baudrate) {
			case 9600:
			case 19200:
			case 38400:
			case 57600:
				break;
			default:
				fprintf(stderr, "invalid baudrate '%s'.\n", optarg);
				exit(1);
			}

			break;

		case '?':
			if (optopt == 'c')
				fprintf(stderr, "option -%c requires an argument.\n", optopt);
			else if (isprint(optopt))
				fprintf(stderr, "unknown option '-%c'.\n", optopt);
			else
				fprintf(stderr, "unknown option character '\\x%x'.\n", optopt);

			exit(1);

		default:
			abort();
		}
	}

	if (!port && (ret = find_serial_port(&port)) < 0) {
		fprintf(stderr, "couldn't find a serial port.\n");
		exit(ret);
	}
}

static void signal_cb(int sig)
{
	const char *s = NULL;

	switch (sig) {
	case SIGINT:
		s = "interrupt";
		break;

	case SIGTERM:
		s = "terminate";
		break;
	}

	fprintf(stdout, "\nreceived signal: %s\n", s);
	exit(0);
}

int main(int argc, char **argv)
{
	int ret;

	parse_options(argc, argv);

	if ((ret = act_connect(&act, port, 9600, 0)) != 0) {
		/* TODO: print error message */
		exit(ret);
	}

	signal(SIGINT, signal_cb);
	signal(SIGTERM, signal_cb);

	while (running) {
		if (process() < 0)
			break;
	}

	if ((ret = act_disconnect(act)) != 0)
		; /* TODO: print error message */

	if (port)
		free(port);

	return ret;
}
